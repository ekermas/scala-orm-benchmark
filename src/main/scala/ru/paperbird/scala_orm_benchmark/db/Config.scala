package ru.paperbird.scala_orm_benchmark.db

import java.io.Closeable
import java.sql.Connection
import javax.sql.DataSource

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}

object Config extends Config

trait Config {
  val databaseH2 = "database.h2"

  val config = ConfigFactory.load()

  private val liquibaseConfig = config.getConfig("database.liquibase")

  val databaseProfileName = config.getString("database.profileName")

  val slickConfig = config.getConfig(s"$databaseProfileName.slick")
  val slickConfigName = s"$databaseProfileName.slick"
  val squerylConfig = config.getConfig(s"$databaseProfileName.squeryl")
  val jdbcConfig = config.getConfig(s"$databaseProfileName.jdbc")
  val quillConfig = config.getConfig(s"$databaseProfileName.quill")
  val quillConfigName = s"$databaseProfileName.quill"

  val liquibaseChangelogFilePath = liquibaseConfig.getString("changelogFilePath")
  val liquibaseContexts = liquibaseConfig.getString("contexts")
  val liquibaseDropAll = liquibaseConfig.getBoolean("dropAll")
  val liquibaseDefaultSchemaName = liquibaseConfig.getString("defaultSchemaName")

}

object HikariDs {

  private val jdbcConfig = Config.jdbcConfig

  Class.forName(jdbcConfig.getString("jdbc.driver"))

  private val config = new HikariConfig()

  config.setJdbcUrl(jdbcConfig.getString("jdbc.url"))
  config.setUsername(jdbcConfig.getString("jdbc.user"))
  config.setPassword(jdbcConfig.getString("jdbc.password"))

  config.setMaximumPoolSize(jdbcConfig.getInt("jdbc.maximumPoolSize"))
  config.setMinimumIdle(jdbcConfig.getInt("jdbc.minimumIdle"))

  //config.setAutoCommit(false)
  config.setAutoCommit(true)

  config.addDataSourceProperty("cachePrepStmts", "true")
  config.addDataSourceProperty("prepStmtCacheSize", "250")
  config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
  config.setCatalog("benchmark")

  private val datasource = new HikariDataSource(config)

  def getConnection(): Connection = datasource.getConnection

  def getDataSource(): DataSource with Closeable = datasource

}