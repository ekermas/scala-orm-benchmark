package ru.paperbird.scala_orm_benchmark.db

import ru.paperbird.scala_orm_benchmark.db.domain.User

import scala.concurrent.Future

trait Repository {

  def getAllUsers(): Seq[User]

  def getUser(user: User): User

  def insertUser(user: User): User

  def updateUser(user: User): User

  def deleteUser(user: User): Int
}
