package ru.paperbird.scala_orm_benchmark.db

import java.sql.Connection

import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor

import ru.paperbird.scala_orm_benchmark.db.Config._


object DatabaseMigrationService {

  private def createLiquibase(dbConnection: Connection, changelogFilePath: String): Liquibase = {
    val database = DatabaseFactory.getInstance.findCorrectDatabaseImplementation(new JdbcConnection(dbConnection))
    database.setDefaultSchemaName(liquibaseDefaultSchemaName)
    new Liquibase(changelogFilePath, new ClassLoaderResourceAccessor(), database)
  }

  private def updateDb(dbConnection: Connection, diffFilePath: String, contexts: String = liquibaseContexts): Unit = {
    val liquibase = createLiquibase(dbConnection, diffFilePath)
    try {
      if (liquibaseDropAll) liquibase.dropAll(liquibase.getDatabase.getDefaultSchema)
      liquibase.update(contexts)
    } catch {
      case e: Throwable => throw e
    } finally {
      dbConnection.commit()
      dbConnection.close()
    }
  }

  def run(dbConnection: Connection): Unit = {
    updateDb(dbConnection, liquibaseChangelogFilePath)
  }

  def run(dbConnection: Connection, contexts: String): Unit = {
    updateDb(dbConnection, liquibaseChangelogFilePath, contexts)
  }
}


