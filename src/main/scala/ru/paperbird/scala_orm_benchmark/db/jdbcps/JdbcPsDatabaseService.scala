package ru.paperbird.scala_orm_benchmark.db.jdbcps

import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.{HikariDs, Repository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class JdbcPsDatabaseService extends Repository {

  def getConnection() = HikariDs.getConnection()

  override def getAllUsers(): Seq[User] = {
    val connection = getConnection()
    val statement = connection.prepareStatement("SELECT id, login, password FROM users")
    val resultSet = statement.executeQuery
    val it = new Iterator[(String, String, String)] {
      def hasNext = resultSet.next()

      def next() = (resultSet.getString(1), resultSet.getString(2), resultSet.getString(3))
    }.toStream
    connection.close()
    it.map(t => User.tupled(t))
  }

  override def getUser(user: User): User = {
    val connection = getConnection()
    val statement = connection.prepareStatement("SELECT id, login, password FROM users WHERE id = ?")
    statement.setString(1, user.id)
    val resultSet = statement.executeQuery
    val it = new Iterator[(String, String, String)] {
      def hasNext = resultSet.next()

      def next() = (resultSet.getString(1), resultSet.getString(2), resultSet.getString(3))
    }.toStream
    connection.close()
    it.map(t => User.tupled(t)).head
  }

  override def insertUser(user: User): User = {
    val connection = getConnection()
    val statement = connection.prepareStatement("insert into USERS (ID, LOGIN, PASSWORD) values (?, ?, ?)")
    statement.setString(1, user.id)
    statement.setString(2, user.login)
    statement.setString(3, user.password)
    val resultSet = statement.executeUpdate()
    connection.close()
    user
  }

  override def updateUser(user: User): User = {
    val connection = getConnection()
    val statement = connection.prepareStatement("update USERS set LOGIN = ?, PASSWORD = ? where ID = ?")
    statement.setString(1, user.login)
    statement.setString(2, user.password)
    statement.setString(3, user.id)
    val resultSet = statement.executeUpdate()
    connection.close()
    user
  }

  override def deleteUser(user: User): Int = {
    val connection = getConnection()
    val statement = connection.prepareStatement(s"DELETE FROM USERS where ID = ?")
    statement.setString(1, user.id)
    val resultSet = statement.executeUpdate()
    connection.close()
    resultSet
  }

}
