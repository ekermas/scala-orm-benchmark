package ru.paperbird.scala_orm_benchmark.db.jdbc

import java.io.Closeable
import java.sql.Connection

import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.{HikariDs, Repository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class JdbcDatabaseService extends Repository {

  def getConnection() = HikariDs.getConnection()

  override def getAllUsers(): Seq[User] = {
    val connection = getConnection()
    val statement = connection.createStatement()
    val resultSet = statement.executeQuery("SELECT id, login, password FROM users")
    val it = new Iterator[(String, String, String)] {
      def hasNext = resultSet.next()

      def next() = (resultSet.getString(1), resultSet.getString(2), resultSet.getString(3))
    }.toStream
    //    connection.commit()
    connection.close()
    it.map(t => User.tupled(t))
  }

  override def getUser(user: User): User = {
    val connection = getConnection()
    val resultSet = connection.createStatement().executeQuery(s"SELECT id, login, password FROM users WHERE id = '${user.id}'")
    val it = new Iterator[(String, String, String)] {
      def hasNext = resultSet.next()

      def next() = (resultSet.getString(1), resultSet.getString(2), resultSet.getString(3))
    }.toStream
    val res = it.map(t => User.tupled(t)).head
    //connection.commit()
    connection.close()
    res
  }

  override def insertUser(user: User): User = {
    val connection = getConnection()
    val res = connection.createStatement().executeUpdate(s"INSERT INTO users (id, login, password) " +
      s"values ('${user.id}', '${user.login}', '${user.password}')")
    //    connection.commit()
    connection.close()
    user
  }

  override def updateUser(user: User): User = {
    val connection = getConnection()
    val resultSet = connection.createStatement().executeUpdate(s"UPDATE users SET " +
      s"login = '${user.login}', password = '${user.password}' WHERE id = '${user.id}'")
    //    connection.commit()
    connection.close()
    user
  }

  override def deleteUser(user: User): Int = {
    val connection = getConnection()
    val resultSet = connection.createStatement().executeUpdate(s"DELETE FROM users WHERE id = '${user.id}'")
    //    connection.commit()
    connection.close()
    resultSet
  }

}
