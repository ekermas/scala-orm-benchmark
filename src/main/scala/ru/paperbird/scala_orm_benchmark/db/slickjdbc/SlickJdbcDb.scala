package ru.paperbird.scala_orm_benchmark.db.slickjdbc

import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.{Config, DatabaseMigrationService, HikariDs, Repository}
import slick.jdbc.GetResult

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

trait DBComponent {

  import slick.basic.DatabaseConfig
  import slick.jdbc.JdbcProfile

  val dc = DatabaseConfig.forConfig[JdbcProfile](Config.slickConfigName)

  val driver: JdbcProfile = dc.profile

  val db = slick.jdbc.JdbcBackend.Database.forDataSource(HikariDs.getDataSource(), Some(20))

}


class SlickJdbcDatabaseService extends DBComponent with Repository {

  import java.sql.Connection
  import driver.api._

  def migrate() = {
    var conn: Option[Connection] = None
    try {
      conn = Some(db.createSession.conn)
      DatabaseMigrationService.run(conn.getOrElse(throw new Exception("Connection is null")))
    } catch {
      case e: Throwable =>
        println(e.getMessage, e)
    } finally {
      if (conn.isDefined) {
        conn.get.close()
      }
    }
  }

  implicit val getUserResult = GetResult[User](r => User(r.nextString, r.nextString, r.nextString))

  override def getAllUsers(): Seq[User] = Await.result({
    db.run(sql"""select * from "USERS"""".as[User]).map(_.toSeq)
  }, 360.seconds)

  override def getUser(user: User): User = Await.result({
    db.run(sql"""select * from "USERS" where "ID" = ${user.id}""".as[User].head)
  }, 360.seconds)

  override def insertUser(user: User): User = Await.result({
    db.run(
      sqlu"""insert into "USERS" ("ID", "LOGIN", "PASSWORD")
            values(${user.id}, ${user.login}, ${user.password})""").map(res => user)
  }, 360.seconds)

  override def updateUser(user: User): User = Await.result({
    db.run(sqlu"""update "USERS" set "LOGIN" = ${user.login}, "PASSWORD" = ${user.password} where "ID" = ${user.id}""").map(r => user)
  }, 360.seconds)

  override def deleteUser(user: User): Int = Await.result({
    db.run(sqlu"""delete FROM "USERS" where "ID" = ${user.id}""")
  }, 360.seconds)
}
