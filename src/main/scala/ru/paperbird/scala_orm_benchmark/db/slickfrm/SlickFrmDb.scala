package ru.paperbird.scala_orm_benchmark.db.slickfrm

import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.slickjdbc.DBComponent
import ru.paperbird.scala_orm_benchmark.db.{Config, DatabaseMigrationService, Repository}

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


class SlickFrmDatabaseService extends DBComponent with Repository {

  import java.sql.Connection
  import driver.api._

  def migrate() = {
    var conn: Option[Connection] = None
    try {
      conn = Some(db.createSession.conn)
      DatabaseMigrationService.run(conn.getOrElse(throw new Exception("Connection is null")))
    } catch {
      case e: Throwable =>
        println(e.getMessage, e)
    } finally {
      if (conn.isDefined) {
        conn.get.close()
      }
    }
  }

  class UserTable(tag: Tag) extends Table[User](tag, "USERS") {

    def id = column[String]("ID", O.PrimaryKey)

    def login = column[String]("LOGIN")

    def password = column[String]("PASSWORD")

    def * = (id, login, password) <> (User.tupled, User.unapply)
  }

  lazy val users = TableQuery[UserTable]

  override def getAllUsers(): Seq[User] = Await.result({
    db.run(users.result)
  }, 360.seconds)

  override def getUser(user: User): User = Await.result({
    db.run(users.filter(_.id === user.id).result.head)
  }, 360.seconds)

  override def insertUser(user: User): User = Await.result({
    db.run(users += user).map(res => user)
  }, 360.seconds)

  override def updateUser(user: User): User = Await.result({
    val query = for {i <- users if i.id === user.id} yield (i.login, i.password)
    db.run(query.update(user.login, user.password)).map(res => user)
  }, 360.seconds)

  override def deleteUser(user: User): Int = Await.result({
    val q = users.filter(_.id === user.id)
    db.run(q.delete)
  }, 360.seconds)
}
