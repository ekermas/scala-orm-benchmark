package ru.paperbird.scala_orm_benchmark.db.squeryl

import java.util.UUID

import org.squeryl._
import org.squeryl.annotations.Column
import org.squeryl.internals.DatabaseAdapter
import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.squeryl.SquerylModel.SQRLUser
import ru.paperbird.scala_orm_benchmark.db.{Config, HikariDs, Repository}

import scala.language.implicitConversions

object SquerylModel extends PrimitiveTypeMode {

  case class SQRLUser(id: String, login: String, password: String) {
    def this() = this(UUID.randomUUID().toString, "", "")
  }

}

import org.squeryl.PrimitiveTypeMode._

object SquerylDatabaseService {
  def apply(): SquerylDatabaseService = {
    new SquerylDatabaseService()
  }
}

class SquerylDatabaseService extends Schema with Repository {

  override def columnNameFromPropertyName(pname: String) = {
    pname.toUpperCase
  }

  val config = Config.squerylConfig

  Class.forName(config.getString("jdbc.driver"))

  val adapter = Class.forName(config.getString("adapter.class")).newInstance().asInstanceOf[DatabaseAdapter]

  val jdbcUrl = config.getString("jdbc.url")

  def getConnection() = HikariDs.getConnection()

  def getSession: AbstractSession = SessionFactory.concreteFactory.get()

  protected def useSession[A](a: => A): A = {
    val session = getSession
    val r = using[A](session) {
      a
    }
    session.connection.close()
    session.close
    r
  }

  SessionFactory.concreteFactory = Some(() => Session.create(getConnection(), adapter))

  val users = table[SquerylModel.SQRLUser]("USERS")

  def toUser(src: SQRLUser): User = User(src.id, src.login, src.password)

  def toSQRLUser(src: User): SQRLUser = SQRLUser(src.id, src.login, src.password)

  override def getAllUsers(): Seq[User] = useSession {
    val res =
      users.allRows.toSeq
    res.map(toUser)
  }

  override def getUser(user: User): User = useSession {
    val res =
      users.where(u => u.id === user.id).single
    toUser(res)
  }

  override def insertUser(user: User): User = useSession {
    val res = users.insert(toSQRLUser(user))
    toUser(res)
  }

  override def updateUser(user: User): User = useSession {
    update(users)(u =>
      where(u.id === user.id)
        set(u.login := user.login,
        u.password := user.password)
    )
    user
  }

  override def deleteUser(user: User): Int = useSession {
    val res =
      users.deleteWhere(u => u.id === user.id)
    res
  }

}
