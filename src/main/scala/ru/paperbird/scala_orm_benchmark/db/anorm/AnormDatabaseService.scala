package ru.paperbird.scala_orm_benchmark.db.anorm

import anorm._
import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.{HikariDs, Repository}


class AnormDatabaseService extends Repository {

  implicit def getConnection = HikariDs.getConnection()

  val baseSelect = "select u.ID, u.LOGIN, u.PASSWORD from USERS u"

  val parser: RowParser[User] = Macro.namedParser[User]

  override def getAllUsers(): Seq[User] = {
    val res = SQL(baseSelect)
      .executeQuery()
      .as(parser.*)
    println(">>>>>" + res.mkString("\n"))
    res
  }

  override def getUser(user: User): User = {
    SQL(baseSelect + " where u.ID = {id}")
      .on('id -> user.id)
      .as(parser.*)
      .head
  }

  override def insertUser(user: User): User = {
    println(">>>>>" + "<<<<")
    SQL("insert into USERS (id, login, password) values ({id}, {login}, {password})")
      .on('id -> user.id, 'login -> user.login, 'password -> user.password)
      .executeUpdate()
    user
  }

  override def updateUser(user: User): User = {
    SQL("update USERS set login = {login}, password = {password} where id = {id}")
      .on('id -> user.id, 'login -> user.login, 'password -> user.password)
      .executeUpdate()
    user
  }

  override def deleteUser(user: User): Int = {
    SQL("delete USERS where id = {id}")
      .on('id -> user.id)
      .executeUpdate()
  }
}
