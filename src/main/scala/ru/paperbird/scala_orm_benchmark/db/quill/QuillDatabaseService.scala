package ru.paperbird.scala_orm_benchmark.db.quill

import io.getquill._
import ru.paperbird.scala_orm_benchmark.db.domain.User
import ru.paperbird.scala_orm_benchmark.db.{HikariDs, Repository}


object ctx extends PostgresJdbcContext(
  NamingStrategy(SnakeCase, UpperCase, PostgresEscape), HikariDs.getDataSource()
) with ImplicitQuery

class QuillDatabaseService extends Repository {

  import ctx._

  private val userSchema = quote {
    querySchema[User]("Users")
  }

  override def getAllUsers(): Seq[User] = {
    val q = quote {
      userSchema
    }
    ctx.run(q)
  }

  override def getUser(user: User): User = {
    val q = quote {
      userSchema.filter(u => u.id == lift(user.id)).take(1)
    }
    ctx.run(q).head
  }

  override def insertUser(user: User): User = {
    val q = quote {
      userSchema.insert(lift(user))
    }
    ctx.run(q)
    user
  }

  override def updateUser(user: User): User = {
    val q = quote {
      userSchema.filter(u => u.id == lift(user.id)).update(lift(user))
    }
    ctx.run(q)
    user
  }

  override def deleteUser(user: User): Int = {
    val q = quote {
      userSchema.filter(u => u.id == lift(user.id)).delete
    }
    ctx.run(q).toInt
  }

}
