package ru.paperbird.scala_orm_benchmark

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import ru.paperbird.scala_orm_benchmark.Benchmark.DatabaseServiceBenchmark
import ru.paperbird.scala_orm_benchmark.db.anorm.AnormDatabaseService
import ru.paperbird.scala_orm_benchmark.db.jdbc.JdbcDatabaseService
import ru.paperbird.scala_orm_benchmark.db.jdbcps.JdbcPsDatabaseService
import ru.paperbird.scala_orm_benchmark.db.quill.QuillDatabaseService
import ru.paperbird.scala_orm_benchmark.db.slickfrm.SlickFrmDatabaseService
import ru.paperbird.scala_orm_benchmark.db.slickjdbc.SlickJdbcDatabaseService
import ru.paperbird.scala_orm_benchmark.db.squeryl.SquerylDatabaseService
import ru.paperbird.scala_orm_benchmark.db.{DatabaseMigrationService, HikariDs}


class CommonSpec extends FlatSpec with Matchers with BeforeAndAfterAll with ScalaFutures {

  val jdbc = new JdbcDatabaseService()
  val jdbcPs = new JdbcPsDatabaseService()
  val quill = new QuillDatabaseService()
  val slickFrm = new SlickFrmDatabaseService()
  val slickJdbc = new SlickJdbcDatabaseService()
  val squeryl = new SquerylDatabaseService()
  val anorm = new AnormDatabaseService()
  val jdbcBenchmark = new DatabaseServiceBenchmark(jdbc)
  val jdbcPsBenchmark = new DatabaseServiceBenchmark(jdbcPs)
  val quillBenchmark = new DatabaseServiceBenchmark(quill)
  val slickFrmBenchmark = new DatabaseServiceBenchmark(slickFrm)
  val slickJdbcBenchmark = new DatabaseServiceBenchmark(slickJdbc)
  val squerylBenchmark = new DatabaseServiceBenchmark(squeryl)
  val anormlBenchmark = new DatabaseServiceBenchmark(anorm)

  override def beforeAll() = {
    DatabaseMigrationService.run(HikariDs.getConnection())
  }

  val limit = 1001
  val step = 50
  val benchRuns = 1

  "Benchmark" should "run jdbc" in {
    jdbcBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }

  it should "run jdbcPs" in {
    jdbcPsBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }

  it should "run quill" in {
    quillBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }

  it should "run slickJdbc" in {
    slickJdbcBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }

  it should "run slickFrm" in {
    slickFrmBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }

  it should "run squeryl" in {
    squerylBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }

  it should "run anorm" in {
    squerylBenchmark.testCaseForScalaTest(limit, step, benchRuns)
  }
}
