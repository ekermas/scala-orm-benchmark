package ru.paperbird.scala_orm_benchmark

import java.util.UUID

import org.scalameter.api._
import org.scalameter.picklers.noPickler._
import ru.paperbird.scala_orm_benchmark.db.Repository
import ru.paperbird.scala_orm_benchmark.db.domain.User

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps


object Benchmark {

  class DatabaseServiceBenchmark(databaseService: Repository) {

    private def testCase(users: Seq[User]): Seq[Int] = {
      val futureResult = users.map { u => {
        val iu = databaseService.insertUser(u)
        val uu = databaseService.getUser(iu)
        databaseService.deleteUser(uu)
      }
      }
      futureResult.map(_ => 1)
    }

    def testCaseForScalaTest(limit: Int, step: Int = 1, benchRuns: Int = 1): Unit = {

      val totalStart = java.lang.System.currentTimeMillis

      (1 to limit by step).foreach { usersCount =>

        println(s"START\t$usersCount")

        val res = mutable.ListBuffer[Float]()

        val users = (1 to usersCount).map { iii =>
          User(UUID.randomUUID().toString, "login", "password")
        }

        (1 to benchRuns).foreach { benchRun =>

          val start = java.lang.System.currentTimeMillis

          testCase(users)

          val end = java.lang.System.currentTimeMillis
          val seconds = (end - start) / 1000f
          res += seconds

          println(s"\tbenchRun $benchRun of $benchRuns: $seconds s")
        }

        val totalSeconds = res.sum / benchRuns
        println(s"END\t$usersCount:\t$totalSeconds s")
      }

      val totalEnd = java.lang.System.currentTimeMillis
      val totalSeconds = (totalEnd - totalStart) / 1000f
      println(s"\n\n########################################")
      println(s"${totalEnd.getClass.getSimpleName} total $totalSeconds s")
      println(s"\n\n########################################")
    }
  }

}