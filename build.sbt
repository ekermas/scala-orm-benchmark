name := "scala-orm-benchmark"

version := "0.0.1"

scalaVersion := "2.12.4"

lazy val commonDependencies = Seq(
  "com.typesafe" % "config" % "1.3.1",
  "ch.qos.logback" % "logback-classic" % "1.1.7"
)

lazy val dbDependencies = Seq(
  "org.liquibase" % "liquibase-core" % "3.5.3",

  "com.typesafe.slick" % "slick_2.12" % "3.2.1",

  "org.squeryl" % "squeryl_2.12" % "0.9.9",

  "io.getquill" % "quill-core_2.12" % "2.3.1",
  "io.getquill" % "quill-jdbc_2.12" % "2.3.1",
  "io.getquill" % "quill-sql_2.12" % "2.3.1",

  "com.typesafe.play" % "anorm_2.12" % "2.5.3",

  "com.zaxxer" % "HikariCP" % "2.7.2",

  "org.postgresql" % "postgresql" % "42.1.4"
)

lazy val testDependencies = Seq(
  "org.scalatest" % "scalatest_2.12" % "3.0.4" % "test",
  "com.storm-enroute" %% "scalameter" % "0.8.2" % "test"
)

libraryDependencies ++= commonDependencies ++ dbDependencies ++ testDependencies